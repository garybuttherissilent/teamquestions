import requests

from api.common import get_authentication

_USERS_URL = "http://127.0.0.1:8000/users/"
_TOTO_ID = 0


def test_list():
    response = requests.get(url=_USERS_URL)

    assert response.status_code == 200
    data = response.json()

    assert type(data) is dict

    assert "count" in data
    assert data["count"] == 1

    assert "next" in data
    assert "previous" in data
    assert "results" in data

    records = data["results"]

    assert type(records) is list
    assert len(records) == data["count"]

    question = records[0]
    assert question["id"] == 2
    assert question["username"] == "vindevoy"


def test_get():
    response = requests.get(url=f"{_USERS_URL[0:-1]}/2/")
    assert response.status_code == 200
    data = response.json()

    assert data["id"] == 2
    assert data["username"] == "vindevoy"
    assert data["first_name"] == "Yves"
    assert data["last_name"] == "Vindevogel"


def test_update():
    response = requests.get(url=f"{_USERS_URL}{_TOTO_ID}/")
    assert response.status_code == 200
    data = response.json()

    assert data["is_superuser"] is False
    data["is_superuser"] = True

    response = requests.put(f"{_USERS_URL}{_TOTO_ID}/", data, auth=get_authentication())

    assert response.status_code == 200

    response = requests.get(url=f"{_USERS_URL}{_TOTO_ID}/")
    assert response.status_code == 200
    data = response.json()
    assert data["is_superuser"] is True


def test_delete():
    response = requests.delete(url=f"{_USERS_URL}{_TOTO_ID}/")
    assert response.status_code == 204


def test_new():
    global _TOTO_ID

    new_user = {
        "password": "toto",
        "is_superuser": False,
        "username": "toto",
        "first_name": "toto",
        "last_name": "le héro",
        "email": "toto@asynchrone.com",
        "is_staff": False,
        "is_active": True
    }

    response = requests.post(_USERS_URL, new_user, auth=get_authentication())
    data = response.json()

    _TOTO_ID = data["id"]


if __name__ == "__main__":
    test_list()
    test_new()
    test_get()
    test_update()
    test_delete()
